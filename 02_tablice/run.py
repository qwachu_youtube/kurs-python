# -*- coding: utf-8 -*-
"""
    Lekcja 2 - Tablice, krotki, zbiory, słowniki
"""
'''
    Tablice (list)
'''
tab = [1, 2, 3, 'a', 'b', 'c']
print('list =>', tab)
print('list[2] =>', tab[2])

# slicing
# print('list[2:5] =>', tab[2:5])
# print('list[2:] =>', tab[2:])
# print('list[:5] =>', tab[:5])
# print('list[-2:] =>', tab[-2:])
# print('list[:-5] =>', tab[:-5])
# print('list[:] =>', tab)

# appending
# tab = []
# tab.append('a')
# tab.append(1)
# tab.append('b')
# tab.append(2)
# tab.append('c')
# tab.append(3)
# print('list =>', tab)

# basic operations
# print('len =>', len(tab))
# print('concat =>', tab + ['x', 'y', 'z', ])
# print('repetition(3x) =>', tab * 3)
# print('membership(3) =>', 3 in tab)
# print('membership(Q) =>', 'Q' in tab)

# iteration
# for i in tab:
#     print(i)

# min/max
# tab = [1, 2, 3]
# print('min =>', min(tab))
# print('max =>', max(tab))


'''
    Krotki (tuple)
'''
# tup = (1, 2, 3, 'a', 'b', 'c')
# print('tuple =>', tup)
# tup = 1, 2, 3, 'a', 'b', 'c'
# print('tuple =>', tup)

# tup[0] = 100  # BAD
# tup2 = (100,)
# tup = tup + tup2
# print('tuple =>', tup)
# print('tuple[2] =>', tup[2])
# print('tuple[2:5] =>', tup[2:5])
# print('tuple[2:] =>', tup[2:])
# print('tuple[:5] =>', tup[:5])
# print('tuple[-2:] =>', tup[-2:])
# print('tuple[:-5] =>', tup[:-5])

# for i in tup:
#     print(i)

# print(tuple([1, 2, 3]))


'''
    Zbiory (set)
'''
# s = {1, 2, 3, 1, 2, 3}
# print('set =>', s)
# print('set[2] =>', s[2])  # BAD
# s2 = set([3, 4, 5])
# print('set2 =>', s2)
# print('intersection =>', s.intersection(s2))
# print('symmetric_difference =>', s.symmetric_difference(s2))
# print('symmetric_difference =>', s2.symmetric_difference(s))
# print('difference =>', s.difference(s2))
# print('difference =>', s2.difference(s))
# print('union =>', s2.union(s))

# tab = [1, 2, 3, 1, 2, 3]
# print('list =>', list(set(tab)))


'''
    Słowniki (dict)
'''
# dictionary = {'a': 1, 'b': 2, 'c': 3, 'a': 999, 'b': 2, 'c': 3}
# print('dictionary =>', dictionary)

# for i in dictionary:
#     print(i)

# for i in dictionary.keys():
#     print(i)

# for i in dictionary.values():
#     print(i)

# for i in dictionary.items():
#     print(i)

# for k, v in dictionary.items():
#     print(k, v)

# print('dictionary[a] =>', dictionary['a'])
# print('dictionary[A] =>', dictionary['A'])  # ERROR
# print('dictionary[z] =>', dictionary['z'])  # ERROR
# print('dictionary[a] =>', dictionary.get('a'))
# print('dictionary[z] =>', dictionary.get('z'))  # NO ERROR
# print('dictionary[z] =>', dictionary.get('z', 999))  # NO ERROR
