# -*- coding: utf-8 -*-
"""
    Lekcja 3 - Operacje na ciągach znaków
"""
'''
    Counting
'''
zdanie = 'Zażółć gęślą jaźń'
print('len()', len(zdanie))
print("count('a')", zdanie.count('a'))
'''
    Case-changes
'''
# zdanie = 'Zażółć gęślą jaźń'
# print('lower()', zdanie.lower())
# print('upper()', zdanie.upper())
# zdanie = 'zażółć gęślą jaźń'
# print('capitalize()', zdanie.capitalize())
'''
    Finding
'''
# zdanie = 'zażółć gęślą jaźń'
# print("find('a')", zdanie.find('a'))
# print("index('a')", zdanie.index('a'))
# print("find('A')", zdanie.find('A'))  # GOOD: -1
# print("index('A')", zdanie.index('A'))  # BAD: ValueError
# print("startswith('za')", zdanie.startswith('za'))
# print("startswith('ZA')", zdanie.startswith('ZA'))
# print("endswith('źń')", zdanie.endswith('źń'))
# print("endswith('ł')", zdanie.endswith('ł'))
'''
    Trimming
'''
# nadmiar = '   Nadmiarowe spacje    '
# print('strip() ', nadmiar.strip())
# print('lstrip()', nadmiar.lstrip())
# print('rstrip()', nadmiar.rstrip())
# nadmiar = '*** Nadmiarowe znaki ***'
# print("strip('*') ", nadmiar.strip('*'))
# print("lstrip('*')", nadmiar.lstrip('*'))
# print("rstrip('*')", nadmiar.rstrip('*'))
'''
    Centering
'''
# zdanie = 'Zażółć gęślą jaźń'
# zdanie2 = 'Ala ma kota.'
# print('center(40)', '*' * 40)
# print('center(40)', zdanie.center(40))
# print('center(40)', zdanie2.center(40))
# print('center(40)', '*' * 40)
'''
    Splitting & joining
'''
# zdanie = 'Zażółć gęślą jaźń'
# tablica_slow = zdanie.split(' ')
# print("split()", tablica_slow)
# print("join()", ', '.join(tablica_slow))
# print("join()", ' '.join(zdanie))
