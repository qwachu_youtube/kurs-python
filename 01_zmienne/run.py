# -*- coding: utf-8 -*-
"""
    Lekcja 1 - Zmienne
"""

"""
    INTEGERS
"""
# zmienna = 7  # GOOD
# print("zmienna = 7 =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = int(7)  # NOT BAD
# print("zmienna = 7 =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = int('7a')  # BAD -> ValueError
# print("zmienna = int('7a')", zmienna, "| type(zmienna) => ", type(zmienna))

"""
    FLOATS
"""
# zmienna = 3.14  # GOOD
# print("zmienna = 3.14 =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = float(3.14)  # GOOD
# print("zmienna = float(3.14) =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = float(3)  # GOOD
# print("zmienna = float(3) =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = float('3.14')  # GOOD
# print("zmienna = float('3.14') =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = int('3,14')  # BAD -> ValueError
# print("zmienna = int('3,14'", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = int('3a')  # BAD -> ValueError
# print("zmienna = int('3a'", zmienna, "| type(zmienna) => ", type(zmienna))


# zmienna = 3.140
# print(zmienna)  # Print current value
# print('%.20f' % zmienna)  # Format to 20 decimals after period
# print(zmienna)  # Original value is not changed

"""
    STRINGS
"""
# zmienna = 'abcd'  # GOOD
# print("zzmienna = 'abcd' =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = "abcd"  # GOOD
# print('zmienna = "abcd" =>', zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = str("abcd")  # GOOD
# print('zmienna = str("abcd") =>', zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = str(3)  # GOOD
# print("zmienna = str(3) =>", zmienna, "| type(zmienna) => ", type(zmienna))

# zmienna = str(3.14)  # GOOD
# print("zmienna = str(3.14) =>", zmienna, "| type(zmienna) => ", type(zmienna))


# zmienna = 7
# print('zmienna.__str__() =>', zmienna.__str__(), '| type(zmienna) =>', type(zmienna), '| type(zmienna.__str__()) =>', type(zmienna.__str__()))


"""
    OPERATIONS
"""
# a, b = 1, 3  # multi assign
# print('a =>', a, '| b =>', b)

# print('a + b =', a + b, '| type(a + b) =>', type(a + b))  # add int to int

# a = 0.14  # Assign float
# print('a =>', a, '| b =>', b)

# print('a + b =', a + b, '| type(a + b) =>', type(a + b))  # add float to int

# a = 'a'  # Assign str
# print('a =>', a, '| b =>', b)

# print('a + b =', a + b, '| type(a + b) =>', type(a + b))  # add str to int  - BAD!
# print('a + str(b) =', a + str(b), '| type(a + str(b))) =>', type(a + str(b)))  # add str to int  - GOOD


# b = 'b'  # Assign str
# print('a =>', a, '| b =>', b)

# print('a + b =', a + b, '| type(a + b) =>', type(a + b))  # concatate strings
